package com.somta.mybatis.demo.example.mapper;

import com.somta.mybatis.demo.dao.UserDao;
import com.somta.mybatis.demo.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Kevin on 2019/1/11.
 */
public class GetMapperWay {
    public static void main(String[] args) {

        String resource = "mybatis-config.xml";
        try {
            InputStream inputStream = Resources.getResourceAsStream(resource);
            //通过SqlSessionFactoryBuilder创建
            SqlSessionFactory sqlMapper = new  SqlSessionFactoryBuilder().build(inputStream);
            SqlSession session = sqlMapper.openSession();
            try {

                //第二种方式
                UserDao userDao = session.getMapper(UserDao.class);
                User user1 = userDao.getUser(1);
                System.out.println(user1.getName());
            } finally {
                session.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
