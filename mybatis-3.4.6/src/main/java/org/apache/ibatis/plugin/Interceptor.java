/**
 *    Copyright 2009-2019 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.plugin;

import java.util.Properties;

/**
 * 插件接口，是框架提供的一种拓展方式，如果要开发自定义的插件，只需要实现这个接口就可以了
 * @author Clinton Begin
 */
public interface Interceptor {

  /**
   * 执行代理类方法
   * @param invocation
   * @return
   * @throws Throwable
   */
  Object intercept(Invocation invocation) throws Throwable;

  /**
   * 创建代理对象
   * @param target
   * @return
   */
  Object plugin(Object target);

  /**
   * 自定义插件属性值
   * @param properties
   */
  void setProperties(Properties properties);

}
